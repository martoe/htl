https://de.wikipedia.org/wiki/H%C3%B6here_Technische_Lehranstalt#Liste_der_H%C3%B6heren_Technischen_Lehranstalten

[10.10. Tag der Wiener Schulen](https://www.wien.gv.at/bildung/stadtschulrat/tag-der-wiener-schulen.html)

# 3. Rennweg

* https://www.htl.rennweg.at/informationstechnologie/
* https://goo.gl/maps/uATtpRhNiBz
* Fachrichtung Medientechnik bzw. Netzwerktechnik (4./5. Klasse)
* Tag der offenen Tür: 9./10.11.?   25./26.1.

# 3. Ungargasse

* https://www.szu.at/Htl-informationstechnologie-netzwerktechnik/73
* Fachrichtung Netzwerktechnik
* Tag der offenen Tür: 23.11./24.11./18.1.

# 5. Spengergasse

* http://www.spengergasse.at/lehrplan/informatik-0
* Wahlpflichtfächer Business Applications, Game Development, Internet of Things, Operations and Services
* ev. bilingual
* Tag der offenen Tür: ???

# 16. Ottakring

* https://www.htl-ottakring.at/index.php/informationstechnologie-netzwerktechnik-medientechnik-4
* Fachrichtung Medientechnik bzw. Netzwerktechnik (4./5. Klasse)
* Tag der offenen Tür: 9.11./10.11./18.1.

# 20. TGM

* https://www.tgm.ac.at/tagesschule/hit
* Fachrichtung Systemtechnik bzw. Medientechnik (4./5. Klasse)
* Lernbüro: https://www.tgm.ac.at/newscontenthit/568-das-lernbuero-in-der-it-hier-wird-individualisierung-gross-geschrieben
* Tag der offenen Tür: ???

# 22. Donaustadt

* http://www.htl-donaustadt.at/abteilungen/informatik/die-abteilung-stellt-sich-vor/
* http://www.htl-donaustadt.at/abteilungen/informationstechnologie/die-abteilung-stellt-sich-vor/
